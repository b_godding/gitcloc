# README #

gogitcloc is an open source helper script that works with git and Al Danial's CLOC (https://github.com/AlDanial/cloc) to generate code metrics.

It is written in GO which means it can be run as a script or binary files can be generated if desired. There are no external GO dependencies.

### Features ###
* Generates metrics at a target interval and date range. 
* At each interval CLOC will generate:
Total lines of code, comments, and blanks for each language
* Between each interval CLOC diff will generate:
Total changes between the intervals grouped by code, comments, and blanks for each language
* Output as SQL table create and insert statements

The intent is this data can then be imported with a tool like smartplot (https://github.com/BGraco/SmartPlot) for visualization.


### Installation ###

Have CLOC and GIT installed and added to the environmental variables. Run the script.

Arguments:
```
--gitPath       Full path to the GIT repository. Default is the current working directory.
--startTime     Starting date as a unix epoch time. Default is the date of the first git commit
--endTime       Ending date as a unix epoch time. Default is the date of the last git commit
--interval      Interval to run the analysis (seconds). Default interval is one week
```

### Getting Started ###