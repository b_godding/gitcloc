package main

import (
	"bytes"
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"strconv"
	"strings"
	"time"
)

/*
The end result is this will be SQL commands that can be used to create a database for further analysis
*/

type gitCommit struct {
	dateTime int64
	hash     string
}

//This function assumes the current working directory contains the target GIT repo
func getCommitHistory() []gitCommit {
	var commitHistory []gitCommit

	// Get commit history of the current branch?
	// Reverse the order, so the data is chronological (default is reverse chronological)
	// Format the date so it looks like "de917ec Wed Oct 21 08:31:25 2015 -0500"
	args := []string{"log", "--reverse", `--pretty=format:"%H %cd"`}

	gitLogOutput, err := exec.Command("git", args...).CombinedOutput()
	if err != nil {
		fmt.Println(string(gitLogOutput), err)
		os.Exit(1)
	}

	// Parse git history
	// Split output by newline
	for _, commit := range strings.Split(string(gitLogOutput), "\n") {
		//Strip " from the results
		commit = strings.Trim(commit, `"`)
		//Split at the first whitespace
		values := strings.SplitN(commit, " ", 2)
		//Parse commit timestamp
		commitTime, _ := time.Parse("Mon Jan _2 15:04:05 2006 -0700", values[1])
		commitHistory = append(commitHistory, gitCommit{commitTime.Unix(), values[0]})
	}
	return commitHistory
}

type interval struct {
	dateTime        int64
	targetGitCommit gitCommit
}

func mapCommitsToIntervals(allCommits []gitCommit, startTime int64, endTime int64, intervalTime int64) []interval {
	var snapshotCommits []interval

	//If no start time is specified, use the time of the first commit
	if startTime == 0 {
		startTime = allCommits[0].dateTime
	}

	//If no end time is specified, use the time of the last commit
	if endTime == 0 {
		endTime = allCommits[len(allCommits)-1].dateTime
	}

	//The first interval just needs to be a point of reference
	for index, commit := range allCommits {
		if commit.dateTime > startTime {
			if index > 0 {
				snapshotCommits = append(snapshotCommits, interval{startTime, allCommits[index-1]})
			} else {
				snapshotCommits = append(snapshotCommits, interval{startTime, allCommits[0]})
			}
			break
		}
	}

	windowStart := startTime

	for windowStart+intervalTime <= endTime {
		for index, commit := range allCommits {
			//We are passed the start of the window, the previous commit is the closest to the interval start
			if commit.dateTime > windowStart {
				//Check if we had any commit in the interval, if not just blank
				if allCommits[index-1].dateTime < windowStart-intervalTime {
					snapshotCommits = append(snapshotCommits, interval{windowStart, gitCommit{startTime, ""}})
				} else if index > 0 {
					snapshotCommits = append(snapshotCommits, interval{windowStart, allCommits[index-1]})
				} else {
					fmt.Println("This really shouldn't happen")
				}
				break
			}
		}
		windowStart = windowStart + intervalTime
	}

	return snapshotCommits
}

func interfaceToInt(value interface{}) int64 {
	if returnValue, ok := value.(float64); ok {
		return int64(returnValue)
	} else if returnString, ok := value.(string); ok {
		returnValue, _ := strconv.ParseInt(returnString, 10, 64)
		return returnValue
	} else {
		return 0
	}
}

func parseClocResult(clocJson string, snapshotId int, buffer *bytes.Buffer) {
	var dat map[string]interface{}
	if err := json.Unmarshal([]byte(clocJson), &dat); err != nil {
		fmt.Println(clocJson, err)
		return
	}

	for key, value := range dat {
		switch key {
		case "header":
			// Throw the header case away, we don't really care
		default:
			valueMap := value.(map[string]interface{})
			//CLOC results will always use 'count' as the type
			buffer.WriteString(fmt.Sprintf(`INSERT INTO results (language, type, nBlank, nComment, nCode, snapshot_id)`+
				` VALUES ("%[1]s", "count", %[2]d, %[3]d, %[4]d, %[5]d);`,
				key,
				int(valueMap["blank"].(float64)),
				int(valueMap["comment"].(float64)),
				int(valueMap["code"].(float64)),
				snapshotId + 1))
			buffer.WriteString("\n")
		}
	}
}

func parseDiffResult(diffJson string, snapshotId int, buffer *bytes.Buffer) {
	var dat map[string]interface{}
	if err := json.Unmarshal([]byte(diffJson), &dat); err != nil {
		fmt.Println(diffJson, err)
		return
	}

	for modKey, modValue := range dat {
		switch modKey {
		case "header":
			// Throw the header case away, we don't really care
		default:
			for langKey, langValue := range modValue.(map[string]interface{}) {
				valueMap := langValue.(map[string]interface{})
				buffer.WriteString(fmt.Sprintf(`INSERT INTO results (language, type, nBlank, nComment, nCode, snapshot_id)`+
					` VALUES ("%[1]s", "%[2]s", %[3]d, %[4]d, %[5]d, %[6]d);`,
					langKey,
					modKey,
					interfaceToInt(valueMap["blank"]),
					interfaceToInt(valueMap["comment"]),
					interfaceToInt(valueMap["code"]),
					snapshotId + 1))
				buffer.WriteString("\n")
			}
		}
	}
}

func main() {
	// Configure user arguments
	gitPath := flag.String("path", "", "Full path to the GIT repository")
	startTime := flag.Int64("start-date", 0, "Starting date as a unix epoch time")
	endTime := flag.Int64("end-date", 0, "Ending date as a unix epoch time")
	interval := flag.Int64("interval", 604800, "Interval to run the analysis (seconds)")

	// Handle user arguments
	flag.Parse()

	// Change working directory to that of git repo
	if err := os.Chdir(*gitPath); err != nil {
		fmt.Println(err)
		return
	}

	var buffer bytes.Buffer
	// Write SQL table for holding snapshot information
	buffer.WriteString("CREATE TABLE snapshots(id INTEGER PRIMARY KEY AUTOINCREMENT, target_timestamp INTEGER, actual_timestamp INTEGER, commit_hash varchar(255));\n")
	// Write SQL table for holding results
	buffer.WriteString("CREATE TABLE results(id INTEGER PRIMARY KEY AUTOINCREMENT, language varchar(255), type varchar(255), nBlank INTEGER, nComment INTEGER, nCode INTEGER, snapshot_id INTEGER, FOREIGN KEY(snapshot_id) REFERENCES snapshots(id));\n")
	//Get a list of all commits to the current branch
	allCommits := getCommitHistory()
	intervalCommits := mapCommitsToIntervals(allCommits, *startTime, *endTime, *interval)

	// Running CLOC on snapshots
	var prevOutput []byte
	for snapshotId, intervalCommit := range intervalCommits {
		buffer.WriteString(fmt.Sprintf(`INSERT INTO snapshots (target_timestamp, actual_timestamp, commit_hash)`+
			` VALUES (%[1]d, %[2]d, "%[3]s");`,
			intervalCommit.dateTime,
			intervalCommit.targetGitCommit.dateTime,
			intervalCommit.targetGitCommit.hash))
		buffer.WriteString("\n")

		//If there is no data for the window use the data from the previous CLOC run
		if intervalCommit.targetGitCommit.hash == "" {
			parseClocResult(string(prevOutput), snapshotId, &buffer)
		} else {
			args := []string{"-json", "--git", intervalCommit.targetGitCommit.hash}
			fmt.Println("Running CLOC with args: ", args)
			clocOutput, err := exec.Command("cloc", args...).CombinedOutput()
			if err != nil {
				fmt.Println(args, string(clocOutput), err)
				os.Exit(1)
			}
			parseClocResult(string(clocOutput), snapshotId, &buffer)
			prevOutput = clocOutput
		}
	}

	// Running diff on snapshots
	prevCommit := intervalCommits[0]
	for snapshotId, intervalCommit := range intervalCommits {
		if snapshotId == 0 {
			continue
		}

		//If there is no data for the window use the date from the previous CLOC run
		if intervalCommit.targetGitCommit.hash == "" || prevCommit.targetGitCommit.hash == intervalCommit.targetGitCommit.hash {
			// fmt.Println("See above", snapshotId)
			// We skip
		} else {
			args := []string{"-json", "--git", "--diff", prevCommit.targetGitCommit.hash, intervalCommit.targetGitCommit.hash}
			fmt.Println("Running CLOC with args: ", args)
			clocOutput, err := exec.Command("cloc", args...).CombinedOutput()
			if err != nil {
				fmt.Println(args, string(clocOutput), err)
			}

			if len(clocOutput) == 0 {
				continue
			}
			parseDiffResult(string(clocOutput), snapshotId, &buffer)
			prevCommit = intervalCommit
		}
	}

	// Write results to file
	//TODO: Make this an arg
	err := ioutil.WriteFile("output.txt", buffer.Bytes(), 0644)
	if err != nil {
		panic(err)
	}
}
